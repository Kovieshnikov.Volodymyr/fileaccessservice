﻿using System.Collections.Generic;
using System.Linq;
using FileAccess.DAL.Abstract;
using FileAccess.DbModels;
using FileAccess.DtoModels;
using Microsoft.AspNetCore.Mvc;

namespace FileAccess.API.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class AccessLevelsController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public AccessLevelsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public ActionResult<List<AccessLevelDto>> GetAccessLevels()
        {
            var levels = _unitOfWork.AccessLevels.GetAll().ToList();
            
            if(levels.Count == 0)
                return NotFound("No access levels found");

            var levelsDto = new List<AccessLevelDto>();
            foreach (var level in levels)
            {
                levelsDto.Add(new AccessLevelDto
                {
                    Id = level.Id,
                    Name = level.Name
                });
            }

            return Ok(levelsDto);
        }

        [HttpGet("{id:int}")]
        public ActionResult<AccessLevelDto> GetAccessLevel(int id)
        {
            var level = _unitOfWork.AccessLevels.Get(id);

            if(level == null)
                return NotFound($"No Access level with id {id}");

            return Ok(new AccessLevelDto
            {
                Id = level.Id,
                Name = level.Name
            });
        }

        [HttpPost]
        public ActionResult AddAccessLevel(AccessLevelDto accessLevelDto)
        {
            var existingLevel = _unitOfWork.AccessLevels.Get(accessLevelDto.Id);
            
            if(existingLevel != null)
                return Conflict(accessLevelDto);
            
            _unitOfWork.AccessLevels.Add(new AccessLevelDb
            {
                Id = accessLevelDto.Id,
                Name = accessLevelDto.Name
            });
            _unitOfWork.SaveChanges();

            return NoContent();
        }

        [HttpDelete("{id:int}")]
        public ActionResult DeleteLevel(int id)
        {
            var level = _unitOfWork.AccessLevels.Get(id);
            if (level == null)
                return NotFound($"No access level with id {id}");

            _unitOfWork.AccessLevels.Remove(level);
            _unitOfWork.SaveChanges();

            return NoContent();

        }
    }
}