﻿using System.Collections.Generic;
using System.Linq;
using FileAccess.DAL.Abstract;
using FileAccess.DbModels;
using FileAccess.DtoModels;
using Microsoft.AspNetCore.Mvc;

namespace FileAccess.API.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class GroupsController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public GroupsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public ActionResult<List<GroupDto>> GetGroups()
        {
            var groups = _unitOfWork.Groups.GetAll();
            if (!groups.Any())
                return NotFound("No groups were found");

            var groupsResponse = groups
                .Select(@group => new GroupDto {Id = @group.Id, Name = @group.Name, Description = @group.Description})
                .ToList();

            return Ok(groupsResponse);
        }

        [HttpGet("{id:int}")]
        public ActionResult<GroupDto> GetGroup(int id)
        {
            var group = _unitOfWork.Groups.Get(id);

            if (group == null)
                return NotFound($"There is no group with id {id}");

            var groupResponse = new GroupDto
            {
                Id = group.Id,
                Name = group.Name,
                Description = group.Description
            };

            return Ok(groupResponse);
        }

        [HttpPost]
        public ActionResult AddGroup(GroupDto groupDto)
        {
            var existingGroup = _unitOfWork.Groups
                .Find(g => g.Name == groupDto.Name)
                .FirstOrDefault();

            if (existingGroup != null)
                return Conflict(groupDto);

            _unitOfWork.Groups.Add(new GroupDb
            {
                Id = groupDto.Id,
                Name = groupDto.Name,
                Description = groupDto.Description
            });
            _unitOfWork.SaveChanges();

            return NoContent();
        }

        [HttpDelete("{id:int}")]
        public ActionResult DeleteGroup(int id)
        {
            var groupDb = _unitOfWork.Groups
                .Find(g => g.Id == id)
                .SingleOrDefault();

            if (groupDb == null)
                return NotFound($"No group with id {id}");

            if (groupDb.Users.Count > 0)
                return Problem($"Group with id {id} is not empty");

            _unitOfWork.Groups.Remove(groupDb);
            _unitOfWork.SaveChanges();
            
            return NoContent();
        }
    }
}