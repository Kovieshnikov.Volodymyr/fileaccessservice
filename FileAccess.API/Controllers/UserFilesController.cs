﻿using System.Collections.Generic;
using System.Linq;
using FileAccess.DAL.Abstract;
using FileAccess.DtoModels;
using FileAccess.Models;
using Microsoft.AspNetCore.Mvc;

namespace FileAccess.API.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class UserFilesController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserFilesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet("username")]
        public ActionResult<UserFileDto> GetFiles(string username)
        {
            var files = _unitOfWork.UserFiles
                .GetUserFilesWithUsers()
                .Where(f => f.User.UserName == username)
                .ToList();

            if (!files.Any())
                return NotFound($"No files found for user {username}");

            var filesDto = new List<UserFileDto>();
            foreach (var file in files)
                filesDto.Add(file.MapUserFileResponse());

            return Ok(filesDto);
        }

        [HttpGet("username, filename")]
        public ActionResult<UserFileDto> GetFile(string username, string filename)
        {
            var file = _unitOfWork.UserFiles
                .GetUserFilesWithUsers()
                .Where(f => f.User.UserName == username)
                .FirstOrDefault(f => f.Name == filename);

            if (file == null)
                return NotFound($"There is no file {filename} for user {username}");

            var fileDto = file.MapUserFileResponse();
            return Ok(fileDto);
        }

        [HttpPost]
        public ActionResult AddFile(string username, UserFile userFile)
        {
            var existingFile = _unitOfWork.UserFiles.Get(userFile.Id);
            if (existingFile != null)
                return Conflict(userFile);

            var fileDb = userFile.MapUserFileRequest();
            fileDb.UserId = _unitOfWork.Users
                .Find(u => u.UserName == username)
                .SingleOrDefault()?.Id;

            _unitOfWork.UserFiles.Add(fileDb);
            _unitOfWork.SaveChanges();

            var fileResponse = fileDb.MapUserFileResponse();

            return CreatedAtAction(
                nameof(GetFile),
                new {username = username, filename = userFile.Name},
                fileResponse);
        }

        [HttpDelete("{id:int}")]
        public ActionResult DeleteFile(int id)
        {
            var file = _unitOfWork.UserFiles.Get(id);
            if (file == null)
                return NotFound("No such file");
            
            _unitOfWork.UserFiles.Remove(file);
            _unitOfWork.SaveChanges();
            
            return NoContent();
        }
    }
}