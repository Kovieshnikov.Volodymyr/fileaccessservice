﻿using System;
using System.Collections.Generic;
using System.Linq;
using FileAccess.DAL.Abstract;
using FileAccess.DtoModels;
using FileAccess.Models;
using Microsoft.AspNetCore.Mvc;

namespace FileAccess.API.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public UsersController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public ActionResult<List<UserDto>> GetUsers()
        {
            var users = _unitOfWork.Users.GetUsersWithGroupsAndAccess()
                .Select(u => u.MapUserResponse());

            return Ok(users);
        }

        [HttpGet("{lastname}")]
        public ActionResult<UserDto> GetUser(string lastname)
        {
            var user = _unitOfWork.Users
                .GetUsersWithGroupsAndAccess()
                .FirstOrDefault(u => u.LastName == lastname)
                .MapUserResponse();

            if (user == null)
                return NotFound();

            return Ok(user);
        }

        [HttpPost]
        public ActionResult AddUser(User user)
        {
            var existingUser = _unitOfWork.Users
                .Find(u => u.UserName == user.UserName)
                .FirstOrDefault();

            if (existingUser != null)
                return Conflict(user);

            var userDb = user.MapUserRequest();
            _unitOfWork.Users.Add(userDb);
            _unitOfWork.SaveChanges();

            var result = userDb.MapUserResponse();

            return CreatedAtAction(nameof(GetUser), new {lastname = result.LastName}, result);
        }

        [HttpPut("username")]
        public ActionResult UpdateUser(string username, User updatedUser)
        {
            try
            {
                _unitOfWork.Users.UpdateUser(username, updatedUser);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

            _unitOfWork.SaveChanges();

            return NoContent();
        }
        
        
        [HttpDelete("{id:int}")]
        public ActionResult DeleteUser(int id)
        {
            var user = _unitOfWork.Users.Get(id);
            if (user == null)
                return NotFound($"No user with id {id}");

            if (user.UserFiles != null)
                _unitOfWork.UserFiles.RemoveRange(user.UserFiles);

            _unitOfWork.Users.Remove(user);
            _unitOfWork.SaveChanges();

            return NoContent();
        }
    }
}