﻿using FileAccess.DbModels;
using FileAccess.DtoModels;
using FileAccess.Models;

namespace FileAccess.API
{
    public static class EntityExtensions
    {
        public static UserDto MapUserResponse(this UserDb userDb) =>
            new UserDto
            {
                Id = userDb.Id,
                FirstName = userDb.FirstName,
                LastName = userDb.LastName,
                EmailAddress = userDb.EmailAddress,
                UserName = userDb.UserName,

                Group = new Group
                {
                    Id = userDb?.GroupDbId ?? 0,
                    Name = userDb.GroupDb?.Name,
                    Description = userDb.GroupDb?.Description
                },

                AccessLevel = new AccessLevel
                {
                    Id = userDb?.AccessLevelDbId ?? 0,
                    Name = userDb.AccessLevelDb?.Name
                }
            };

        public static UserDb MapUserRequest(this User user) =>
            new UserDb
            {
                Id = 0,
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.UserName,
                EmailAddress = user.EmailAddress,
                AccessLevelDbId = 2,
                GroupDbId = 2
            };

        public static UserFileDto MapUserFileResponse(this UserFileDb userFileDb) =>
            new UserFileDto
            {
                Id = userFileDb.Id,
                Name = userFileDb.Name,
                CreationDate = userFileDb.CreationDate,
                User = new User
                {
                    Id = userFileDb.User.Id,
                    FirstName = userFileDb.User.FirstName,
                    LastName = userFileDb.User.LastName,
                    EmailAddress = userFileDb.User.EmailAddress,
                    UserName = userFileDb.User.UserName
                },
            };

        public static UserFileDb MapUserFileRequest(this UserFile userFile) =>
            new UserFileDb
            {
                Id = 0,
                Name = userFile.Name,
                CreationDate = userFile.CreationDate,
            }; 
    }
}