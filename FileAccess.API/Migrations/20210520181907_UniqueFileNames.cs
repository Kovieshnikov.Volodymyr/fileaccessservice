﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FileAccess.API.Migrations
{
    public partial class UniqueFileNames : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "UserFiles",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.CreateIndex(
                name: "IX_UserFiles_Name",
                table: "UserFiles",
                column: "Name",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_UserFiles_Name",
                table: "UserFiles");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "UserFiles",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}
