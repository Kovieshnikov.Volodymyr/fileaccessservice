﻿using FileAccess.DbModels;

namespace FileAccess.DAL.Abstract
{
    public interface IAccessLevelRepository : IRepository<AccessLevelDb>
    {
        
    }
}