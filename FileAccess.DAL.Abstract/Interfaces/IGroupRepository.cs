﻿using FileAccess.DbModels;

namespace FileAccess.DAL.Abstract
{
    public interface IGroupRepository : IRepository<GroupDb>
    {
        
    }
}