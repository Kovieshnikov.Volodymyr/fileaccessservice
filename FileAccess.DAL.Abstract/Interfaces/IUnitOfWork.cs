﻿using System;

namespace FileAccess.DAL.Abstract
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository Users { get; }
        IUserFileRepository UserFiles {get;} 
        IGroupRepository Groups { get; }
        IAccessLevelRepository AccessLevels { get; }

        int SaveChanges();
    }
}