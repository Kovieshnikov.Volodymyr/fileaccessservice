﻿using System.Collections.Generic;
using FileAccess.DbModels;

namespace FileAccess.DAL.Abstract
{
    public interface IUserFileRepository : IRepository<UserFileDb>
    {
        IEnumerable<UserFileDb> GetUserFilesWithUsers();
    }
}