﻿using System.Collections.Generic;
using FileAccess.DbModels;
using FileAccess.Models;

namespace FileAccess.DAL.Abstract
{
    public interface IUserRepository : IRepository<UserDb>
    {
        IEnumerable<UserDb> GetUsersWithGroupsAndAccess();
        void UpdateUser(string username, User updatedUser);
    }
}