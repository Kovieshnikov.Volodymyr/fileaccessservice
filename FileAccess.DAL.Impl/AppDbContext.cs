﻿using FileAccess.DbModels;
using Microsoft.EntityFrameworkCore;

namespace FileAccess.DAL.Impl
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            :base(options)
        {

        }

        public DbSet<UserDb> Users { get; set; }
        public DbSet<UserFileDb> UserFiles { get; set; }
        public DbSet<AccessLevelDb> AccessLevels { get; set; }
        public DbSet<GroupDb> Groups { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserDb>()
                .HasIndex(u => u.UserName)
                .IsUnique();

            modelBuilder.Entity<UserFileDb>()
                .HasIndex(f => f.Name)
                .IsUnique();
            
        }
    }
}