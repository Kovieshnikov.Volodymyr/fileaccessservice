﻿using FileAccess.DAL.Abstract;
using FileAccess.DbModels;
using Microsoft.EntityFrameworkCore;

namespace FileAccess.DAL.Impl
{
    public class AccessLevelRepository : Repository<AccessLevelDb>, IAccessLevelRepository
    {
        public AccessLevelRepository(DbContext context) : base(context)
        {
        }
    }
}