﻿using FileAccess.DAL.Abstract;
using FileAccess.DbModels;
using Microsoft.EntityFrameworkCore;

namespace FileAccess.DAL.Impl
{
    public class GroupRepository : Repository<GroupDb>, IGroupRepository
    {
        public GroupRepository(DbContext context) : base(context)
        {
        }
    }
}