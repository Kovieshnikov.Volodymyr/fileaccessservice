﻿using FileAccess.DAL.Abstract;

namespace FileAccess.DAL.Impl
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;

        public IUserRepository Users { get; }
        public IUserFileRepository UserFiles {get;} 
        public IGroupRepository Groups { get; }
        public IAccessLevelRepository AccessLevels { get; }
        
        public UnitOfWork(AppDbContext context)
        {
            _context = context;
            Users = new UserRepository(_context);
            UserFiles = new UserFileRepository(_context);
            Groups = new GroupRepository(_context);
            AccessLevels = new AccessLevelRepository(_context);
        }

        public int SaveChanges() => _context.SaveChanges();

        public void Dispose() => _context.Dispose();
    }
}