﻿using System.Collections.Generic;
using System.Linq;
using FileAccess.DAL.Abstract;
using FileAccess.DbModels;
using Microsoft.EntityFrameworkCore;

namespace FileAccess.DAL.Impl
{
    public class UserFileRepository : Repository<UserFileDb>, IUserFileRepository
    {
        public AppDbContext AppDbContext => Context as AppDbContext;
        
        public UserFileRepository(DbContext context) : base(context)
        {
            
        }

        public IEnumerable<UserFileDb> GetUserFilesWithUsers() =>
            AppDbContext.UserFiles
                .AsNoTracking()
                .Include(f => f.User)
                .OrderBy(f => f.CreationDate);
    }
}