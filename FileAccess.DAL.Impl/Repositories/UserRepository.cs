﻿using System;
using System.Collections.Generic;
using System.Linq;
using FileAccess.DAL.Abstract;
using FileAccess.DbModels;
using FileAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace FileAccess.DAL.Impl
{
    public class UserRepository : Repository<UserDb>, IUserRepository
    {
        private AppDbContext AppDbContext => Context as AppDbContext;
        
        public UserRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<UserDb> GetUsersWithGroupsAndAccess() =>
            AppDbContext.Users
                .AsNoTracking()
                .Include(u => u.GroupDb)
                .Include(u => u.AccessLevelDb)
                .OrderBy(u => u.UserName);

        public void UpdateUser(string username, User updatedUser)
        {
            var user = AppDbContext.Users
                .FirstOrDefault(u => u.UserName == username);
            
            if (user == null)
                throw new ArgumentException($"User {username} not found");

            user.FirstName = updatedUser.FirstName;
            user.LastName = updatedUser.LastName;
            user.EmailAddress = updatedUser.EmailAddress;
        }
    }
}