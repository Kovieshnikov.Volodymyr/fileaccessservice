﻿using System.Collections.Generic;
using FileAccess.Models;

namespace FileAccess.DbModels
{
    public class AccessLevelDb : AccessLevel
    {
        public ICollection<UserDb> Users { get; set; }
    }
}