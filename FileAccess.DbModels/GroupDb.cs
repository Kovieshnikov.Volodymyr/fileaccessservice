﻿using System.Collections.Generic;
using FileAccess.Models;

namespace FileAccess.DbModels
{
    public class GroupDb : Group
    {
        public ICollection<UserDb> Users { get; set; }
    }
}