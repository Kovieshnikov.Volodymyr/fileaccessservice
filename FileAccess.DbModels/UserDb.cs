﻿using System.Collections.Generic;
using FileAccess.Models;

namespace FileAccess.DbModels
{
    public class UserDb : User
    {
        public ICollection<UserFileDb> UserFiles { get; set; }
        public AccessLevelDb AccessLevelDb { get; set; }
        public GroupDb GroupDb { get; set; }
        
        public int? AccessLevelDbId { get; set; }
        public int? GroupDbId { get; set; }
    }
}