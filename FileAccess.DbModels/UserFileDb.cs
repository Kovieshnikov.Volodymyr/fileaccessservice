﻿using FileAccess.Models;

namespace FileAccess.DbModels
{
    public class UserFileDb : UserFile
    {
        public UserDb User { get; set; }
        public int? UserId { get; set; }
    }
}