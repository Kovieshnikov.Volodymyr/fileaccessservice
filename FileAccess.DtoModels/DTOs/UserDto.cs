﻿using System.Collections.Generic;
using FileAccess.Models;

namespace FileAccess.DtoModels
{
    public class UserDto : User
    {
        public ICollection<UserFile> UserFiles { get; set; } = new List<UserFile>();
        public AccessLevel AccessLevel { get; set; }
        public Group Group { get; set; }
    }
}