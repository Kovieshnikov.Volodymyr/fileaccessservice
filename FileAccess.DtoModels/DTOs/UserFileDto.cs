﻿using FileAccess.Models;

namespace FileAccess.DtoModels
{
    public class UserFileDto : UserFile
    {
        public User User { get; set; }
    }
}