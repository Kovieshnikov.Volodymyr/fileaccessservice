﻿using System.ComponentModel.DataAnnotations;

namespace FileAccess.Models
{
    public class AccessLevel
    {
        public int Id { get; set; }
        [Required] public string Name { get; set; }
    }
}