﻿using System.ComponentModel.DataAnnotations;

namespace FileAccess.Models
{
    public class User
    {
        public int Id { get; set; }
        [Required] [StringLength(50)] public string FirstName { get; set; }
        [Required] [StringLength(50)] public string LastName { get; set; }
        [Required] [StringLength(50)] public string UserName { get; set; }
        [StringLength(100)] public string EmailAddress { get; set; }
    }
}