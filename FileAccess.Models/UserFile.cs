﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FileAccess.Models
{
    public class UserFile
    {
        public int Id { get; set; }
        [Required] public string Name { get; set; }
        public DateTime? CreationDate { get; set; }
    }
}