﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FileAccess.DtoModels;
using FileAccess.PL.Services;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FileAccess.PL.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IAPIClient _client;

        public IEnumerable<UserDto> Users { get; set; }
        
        public IndexModel(IAPIClient client)
        {
            _client = client;
        }

        public async Task OnGet()
        {
            var usersDto = await _client.GetUsersAsync();

            Users = usersDto;
        }
    }
}
