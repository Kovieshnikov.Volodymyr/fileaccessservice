﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using FileAccess.DtoModels;

namespace FileAccess.PL.Services
{
    public class APIClient : IAPIClient
    {
        private readonly HttpClient _client;

        public APIClient(HttpClient client)
        {
            _client = client;
        }

        public async Task<List<UserDto>> GetUsersAsync()
        {
            var response = await _client.GetAsync("/api/users");

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<List<UserDto>>();
        }
    }
}