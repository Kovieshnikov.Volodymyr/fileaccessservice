﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FileAccess.DtoModels;

namespace FileAccess.PL.Services
{
    public interface IAPIClient
    {
        Task<List<UserDto>> GetUsersAsync();
    }
}